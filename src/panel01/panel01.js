var panel01_elements = [
    {
        patient:{
            src:"./assets/therapy.svg"
        }
    },
    {
        physiotherapist:{
            src:"./assets/running_man.svg"
        }
    }

]


d3.select("#panel01")
.data(panel01_elements)
.enter().append('svg')
.attr("src",(function(d){
    return d.src
}))


